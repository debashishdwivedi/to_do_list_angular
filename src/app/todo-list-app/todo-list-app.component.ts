import { Component } from '@angular/core';

@Component({
  selector: 'todo-list-app',
  template: `<router-outlet></router-outlet>`
})
export class TodoListAppComponent  {
  title = 'To Do List';
 }
